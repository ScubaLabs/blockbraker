﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class brickhit : MonoBehaviour {

	public int maxHit;
	private int hits;
	public Sprite[] arr;
	public static int win;
	private

	// Use this for initialization
	void Start () {

		win = 0;
		hits = 0;
	}

	public void Damage(){

		maxHit--;

		if (maxHit <= 0) {
			win++;
			Destroy (gameObject);
		} else {
			this.GetComponent<SpriteRenderer>().sprite = arr[maxHit-1] ;
		}
	}
}
