﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class paddleMove : MonoBehaviour {

	private float f;
	private float maxX = 14.5f;
	private float minX = 0.5f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		f = Input.GetAxis ("Mouse X");

		if (f != 0) {
			//transform. position = new Vector2(f, 0);
			transform.Translate (f, 0, 0);
		}

		transform. position = new Vector2(Mathf.Clamp (transform. position.x, minX, maxX), 0);
	}
}
