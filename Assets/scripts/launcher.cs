﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class launcher : MonoBehaviour {

	private bool flag = false;
	private GameObject pad;
	private Transform t;
	//private CircleCollider2D cc;
	private Rigidbody2D rb;
	private Vector3 pos;

	private int brick1count = 9;
	private int brick2count = 8;
	private int brick3count = 10;
	private int totalCount;
	public string name;

	// Use this for initialization
	void Start () {

		pad = GameObject.Find("paddle");	//cc = GetComponent<CircleCollider2D> ();
		rb = this.GetComponent<Rigidbody2D> ();

		t = pad.GetComponent<Transform> ();
		pos = this.transform.position - t.position;

		totalCount = brick1count + brick2count + brick3count;

	}
	
	// Update is called once per frame
	void Update () {



		if(flag == false){
			
			this.transform.position = t.position + pos;

			if (Input.GetMouseButtonDown (0)) {

				print ("mouse click");
				rb.velocity = new Vector2 (2, 15);
				flag = true;

				//rb.bodyType = RigidbodyType2D.Dynamic;
				//cc.sharedMaterial.bounciness = 1;
				//bounce.bounciness = 1;
			}
		}
	}

	void OnCollisionEnter2D (Collision2D col){
	
		if (col.collider.GetComponent<brickhit> ()) {
			col.collider.GetComponent<brickhit> ().Damage ();

			if (brickhit.win == totalCount)
				//Debug.Log ("you win");
				SceneManager.LoadScene (name); 	
		}
	}
	}
